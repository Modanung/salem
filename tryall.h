#ifndef TRYALL_H
#define TRYALL_H

#include "court.h"

#include <QMainWindow>

class TryAll : public QMainWindow
{
    Q_OBJECT

public:
    TryAll(QWidget *parent = nullptr);
    ~TryAll();

private:
    Court* court_;
};
#endif // TRYALL_H
