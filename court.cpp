#include <QTimer>
#include <QMouseEvent>

#include "court.h"

Court::Court(QWidget* parent): QWidget(parent),
    room_{ size(), QImage::Format_RGBA8888 },
    jury_{},
    active_{ nullptr },
    from_  { nullptr },
    to_    { nullptr },
    witch_ { new Witch{} },
    animate_{ false }
{
    setAngleUnit(GON);

    setMinimumSize(0500, 0500);
    setMouseTracking(true);

    for (int j{00}; j < UNIT<<1; ++j)
    {
        const bool  stand{  (j  /   UNIT) % 2       };
        const bool  leap {   j  /  (UNIT >> 1) % 2  };
        const int   step {   j  %  (UNIT >> 1)      };
        const float seat { 1.0f / ((UNIT >> 1) + 1) };

        Member* m{ new Member{
                   (1 + (!stand ? step : leap) + (((UNIT / 2 - 1) * leap - 0.5f)
                      *   stand)) * seat ,
                   (1 + (!stand ? leap : step) + (((UNIT / 2 - 1) * leap - 0.5f)
                      *  !stand)) * seat  } };

        if (!stand)
        {
            if (leap)
                m->side_ = Bottom;
            else
                m->side_ = Top;
        }
        else if (leap)
            m->side_ = Right;
        else
            m->side_ = Left;

        m->turn();
        jury_.push_back(m);
    }
    jury_.push_back(&bigwig_);

    from_ = jury_.at(1 + UNIT / 2);
    to_   = jury_.at(1);

    from_->angle_ =  200 - (100 -  Rune::beauty().inDeg());
    to_  ->angle_ = -200 - (Rune::beauty().outDeg() - 100);


    refreshWitch();

    if (animate_)
    {
        QTimer* timer{ new QTimer{ this } };
        connect(timer, SIGNAL(timeout()), this, SLOT(converse()));
        timer->start(TIMESTEP * 1000);
    }
}

void Court::paintEvent(QPaintEvent* e)
{
    drawJury();

    QPainter p{ this };

    p.setCompositionMode(QPainter::CompositionMode_Exclusion);
    p.drawImage(e->rect(), room_.copy(e->rect()));

    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
    p.setRenderHint(QPainter::Antialiasing);
    p.strokePath(*witch_, { Qt::red, UNIT / 2, Qt::SolidLine, Qt::RoundCap  });
}

void Court::drawJury()
{
    room_.fill(0);
    QPainter p{ &room_ };
    p.setPen({Qt::black, 2});
    p.setRenderHint(QPainter::Antialiasing);

    for (Member* j: jury_)
    {
        p.setBrush({ j == active_ ? Qt::white : Qt::gray });
        p.drawEllipse(position(j), UNIT<<1, UNIT<<1);
        QLineF aim{ position(j), position(j) + QPoint{ UNIT<<1, 0 } };

        aim.setAngle(convertAngle<DEG>(j->angle_));
        p.drawLine(aim);
    }

    p.end();
}

void Court::resizeEvent(QResizeEvent* e)
{
    QImage space{ size(), QImage::Format_RGBA8888 };
    space.fill(0);
    room_.swap(space);

    refreshWitch();
}

void Court::mouseMoveEvent(QMouseEvent* e)
{
    Member* hover{ nullptr };

    if (!e->buttons())
    {
        for (Member* j: jury_)
        {
            if (QLineF{ e->pos(), position(j) }.length() < UNIT<<1)
                hover = j;
        }

        if (hover != active_)
        {
            QRect r{};

            if (active_)
                r |= active_->rect(size());

            active_ = hover;

            if (active_)
                r |= active_->rect(size());

            if (!r.isEmpty())
                repaint(r);
        }
    }
    else if (active_)
    {
        setCursor(Qt::BlankCursor);

        QPoint d{ e->pos() - position(active_) };
        float a{ (d.x() + d.y()) * -0.1f  };
        active_->turn(a);

        QRect r{ active_->rect(size()) };

        if (active_ == from_
         || active_ ==   to_)
        {
            refreshWitch();
            repaint();
        }
        else
        {
            repaint(r);
        }

        QCursor::setPos(mapToGlobal(position(active_)));
    }
}

void Court::mousePressEvent(QMouseEvent* e)
{
    if (!active_)
        return;

    if (active_ == &bigwig_)
        return;

    if (e->buttons() & Qt::LeftButton)
    {
        if (to_ == active_)
            to_ = from_;

        from_ = active_;
        refreshWitch();
        repaint();
    }
    else if (e->buttons() & Qt::RightButton)
    {
        if (from_ == active_)
            from_ = to_;

        to_ = active_;
        refreshWitch();
        repaint();
    }
}

void Court::mouseReleaseEvent(QMouseEvent* e)
{
    if (active_ == &bigwig_)
    {
             if (bench_ == Curve)
                 bench_  = Lobe;
        else if (bench_ == Lobe) bench_  = Curve;
//                 bench_  = Flame;
//        else if (bench_ == Flame)
//                 bench_  = Curve;

        refreshWitch();
        repaint();
    }

    if (cursor() == Qt::BlankCursor)
        setCursor(Qt::ArrowCursor);
}

void Court::wheelEvent(QWheelEvent* e)
{
    if (!active_)
        return;

    active_->turn(e->delta() * 0.017f);

    if (active_ == from_ || active_ == to_)
    {
        refreshWitch();
        repaint();
    }
    else
    {
        repaint(active_->rect(size()));
    }
}
